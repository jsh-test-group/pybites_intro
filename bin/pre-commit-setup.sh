#!/bin/bash -eu
# create pre-commit hook to lint and beautify

cat > .pre-commit-config.yaml <<'__EOF__'
repos:
  - repo: local
    hooks:
      - id: autoflake
        name: Remove unused variables and imports
        entry: bash -c 'autoflake "$@"; git add -u' --
        language: python
        args:
          [
            "--in-place",
            "--remove-all-unused-imports",
            "--remove-unused-variables",
            "--expand-star-imports",
            "--ignore-init-module-imports",
          ]
        files: \.py$
      - id: isort
        name: Sorting import statements
        entry: bash -c 'isort "$@"; git add -u' --
        language: python
        args: ["--profile", "black", "--filter-files"]
        files: \.py$
      - id: black
        name: Black Python code formatting
        entry: bash -c 'black "$@"; git add -u' --
        language: python
        types: [python]
        args: ["--line-length=120"]
      - id: mypy
        name: Optional static type checker
        entry: bash -c 'mypy "$@"; git add -u' --
        language: python
        files: \.py$
__EOF__
pre-commit install
