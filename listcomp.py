"""Intro Bite 07. Filter numbers with a list comprehension."""


def filter_positive_even_numbers(numbers):
    """Receives a list of numbers, and returns a filtered list of only the
    numbers that are both positive and even (divisible by 2), try to use a
    list comprehension."""
    return [number for number in numbers if number > 0 and not number % 2]
