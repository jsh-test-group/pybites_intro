"""Unit-test more_dict."""

import pytest

from more_dict import CHILL_OUT, INVALID_DAY, TRAIN, get_workout_motd


@pytest.mark.parametrize(
    "day, expected_output",
    [
        ("nonsense", INVALID_DAY),
        ("friday", TRAIN.format("Shoulders")),
        ("Friday", TRAIN.format("Shoulders")),
        ("FrIdaY", TRAIN.format("Shoulders")),
        ("SUNDAY", CHILL_OUT),
    ],
)
def test_get_workout_motd(day, expected_output) -> None:
    """Unit-test get_workout_motd."""
    assert get_workout_motd(day) == expected_output
