#!/usr/bin/env python3
"""Unit test for loops module."""

from loops import print_colors


def test_print_colors(monkeypatch, capsys):
    """print_colors() tests."""

    # monkeypatch.setattr("builtins.input", lambda _: "blue")
    # print_colors()
    # out, err = capsys.readouterr()
    # assert out == "blue\n"
    # assert err == ""

    monkeypatch.setattr("builtins.input", lambda _: "quit")
    print_colors()
    out, err = capsys.readouterr()
    assert out == "bye\n"
    assert err == ""
