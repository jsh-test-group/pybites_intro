"""Unit tests for slices module."""

import pytest

from slices import slice_and_dice, terminator, text


@pytest.mark.parametrize(
    "observed_input, expected_output",
    [
        ("", None),
        ("This", None),
        ("this", "this"),
        ("  this  ", "this"),
        ("  this!.!!..  ", "this"),
    ],
)
def test_terminator(observed_input, expected_output):
    """test terminator() with parameters."""
    assert terminator(observed_input) == expected_output


def test_slice_and_dice():
    """test slice_and_dice() with parameter."""

    observed_input = "  hello, there.!.!!.\nthis is a line.   \nand this is another."
    expected_output = ["there", "line", "another"]
    assert slice_and_dice(observed_input) == expected_output


def test_slice_and_dice_default():
    """test slice_and_dice() default."""
    assert slice_and_dice(text) == slice_and_dice()
