#!/usr/bin/env python3
"""Unit test for fstrings module."""

import pytest

from fstrings import allowed_driving


@pytest.mark.parametrize(
    "name, age, expected_output",
    [
        ("jeff", 18, "jeff is allowed to drive"),
        ("jeff", 17, "jeff is not allowed to drive"),
        ("kristina", 18, "kristina is allowed to drive"),
    ],
)
def test_allowed(name, age, expected_output, capsys):
    """allowed_driving() unit test."""
    allowed_driving(name, age)
    out, err = capsys.readouterr()
    assert out.strip() == expected_output
    assert err == ""
