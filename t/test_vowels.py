"""Unit tests for vowels.py. """

from typing import Tuple

import pytest

from vowels import strip_vowels

SENTENCE = "The quick brown fox\njumped over the lazy dog."
UC_SENTENCE = SENTENCE.upper()


def vowels_to_stars(string: str) -> Tuple[str, int]:
    """Change vowels to stars, case-insensitive."""
    string = string.replace("a", "*")
    string = string.replace("e", "*")
    string = string.replace("i", "*")
    string = string.replace("o", "*")
    string = string.replace("u", "*")
    string = string.replace("A", "*")
    string = string.replace("E", "*")
    string = string.replace("I", "*")
    string = string.replace("O", "*")
    string = string.replace("U", "*")
    return (string, string.count("*"))


@pytest.mark.parametrize(
    "text, expected_output",
    [
        ("aeiou", ("*****", 5)),
        ("AEIOU", ("*****", 5)),
        (SENTENCE, vowels_to_stars(SENTENCE)),
        (UC_SENTENCE, vowels_to_stars(UC_SENTENCE)),
    ],
)
def test_strip_vowels(text: str, expected_output: Tuple[str, int]):
    """strip_vowels() unit test"""
    assert strip_vowels(text) == expected_output
