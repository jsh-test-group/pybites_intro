#!/usr/bin/env python3
"""Unit test for dictionaries module."""

import pytest

from dictionaries import games_won, print_game_stat, print_game_stats


@pytest.mark.parametrize(
    "name, expected_output",
    [
        ("sara", "sara has won 0 games"),
        ("bob", "bob has won 1 game"),
        ("tim", "tim has won 5 games"),
        ("julian", "julian has won 3 games"),
        ("jim", "jim has won 1 game"),
    ],
)
def test_print_game_stat_name(name, expected_output, capsys):
    """print_game_stat() unit tests."""
    print_game_stat(name, games_won[name])
    out, err = capsys.readouterr()
    assert out.strip() == expected_output
    assert err == ""


def test_print_game_stats(capsys):
    """print_game_stats() unit test."""
    games_stats = dict(jeff=69)
    expected_output = "jeff has won 69 games"
    print_game_stats(games_stats)
    out, err = capsys.readouterr()
    assert out.strip() == expected_output
    assert err == ""
