"""Unit tests for Intro Bite 07: Filter numbers with a list comprehension"""

import pytest

from listcomp import filter_positive_even_numbers


@pytest.mark.parametrize(
    "numbers, expected_output",
    [
        (range(-10, 10), [2, 4, 6, 8]),
        (range(10, -10), []),
        (range(10, -10, -1), [10, 8, 6, 4, 2]),
    ],
)
def test_filter_positive_even_numbers(numbers, expected_output):
    """Test filter_positive_even_numbers()."""
    assert filter_positive_even_numbers(numbers) == expected_output
