#!/usr/bin/env python3
"""Unit tests for split_join module."""

import pytest

from split_join import split_in_columns


@pytest.mark.parametrize(
    "message, expected_output",
    [("", ""), ("hello, world\ngoodbye dennis", "hello, world|goodbye dennis")],
)
def test_split_in_columns(message, expected_output):
    """split_in_columns() unit tests."""
    assert split_in_columns(message) == expected_output


def test_split_in_columns_default():
    """split_in_columns() test default message."""
    expected_output = (
        "Hello world!|We hope that you are learning a lot of Python.|"
        "Have fun with our Bites of Py.|Keep calm and code in Python!"
        "|Become a PyBites ninja!"
    )
    assert split_in_columns() == expected_output
