"""Unit tests for named_tuples.py."""

import pytest

from named_tuples import BeltStats, get_total_points, ninja_belts


@pytest.mark.parametrize(
    "belts, expected_output",
    [
        ({"violet": BeltStats(69, 42)}, 69 * 42),
        ({"red": BeltStats(69, 42), "indigo": BeltStats(1, 1)}, 69 * 42 + 1),
    ],
)
def test_get_total_points_with_args(belts: dict[str, BeltStats], expected_output: int) -> None:
    """Unit test get_total_points with arguments."""
    assert get_total_points(belts) == expected_output


def test_get_total_points_default() -> None:
    """Unit test get_total_points default behavior."""
    assert get_total_points() == get_total_points(ninja_belts)
