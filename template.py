#!/usr/bin/env python3
"""A filter template."""
import argparse


def parse_args() -> argparse.Namespace:
    """Argument parsing."""
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", "-d", action="store_true", help="debugging")
    parser.add_argument("files", nargs="*", type=str, help="files")
    args = parser.parse_args()

    # some defaults
    if not args.files:
        args.files = [0]
    if args.debug:
        print(args)

    print(type(args))
    return args


def main() -> None:
    """The big tent."""
    parse_args()


if __name__ == "__main__":
    main()
