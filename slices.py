"""
Pybites Intro Bite 05: Slice and dice.
"""

from string import ascii_lowercase

text = """
One really nice feature of Python is polymorphism: using the same operation
on different types of objects.
Let's talk about an elegant feature: slicing.
You can use this on a string as well as a list for example
'pybites'[0:2] gives 'py'.
 The first value is inclusive and the last one is exclusive so
here we grab indexes 0 and 1, the letter p and y.
  When you have a 0 index you can leave it out so can write this as 'pybites'[:2]
but here is the kicker: you can use this on a list too!
['pybites', 'teaches', 'you', 'Python'][-2:] would gives ['you', 'Python']
and now you know about slicing from the end as well :)
keep enjoying our bites!
"""


def terminator(line: str):
    """
    strip off any leading spaces,
    check if the first character is lowercase,
    if so, split the line into words and get the last word,
    strip off BOTH the trailing dot (.) and exclamation mark (!) from this last word,
    """
    line = line.strip()
    if line and (line[0] in ascii_lowercase):
        words = line.split()
        last_word = words[-1].rstrip("!.")
        return last_word
    return None


def slice_and_dice(input_text: str = text) -> list:
    """Get a list of words from the passed in text.


    Loop through the lines, for each line:

    pick the word described above
    and add it to the results list.

    Return the results list.
    """

    # Take the block of text provided and strip off the whitespace at both ends.
    # Split the text by newline (\n).
    results = []
    for line in input_text.split("\n"):
        last_word = terminator(line)
        if last_word:
            results.append(last_word)
    return results
