# Just the basics.


## Constants
SRCS := $(wildcard *.py)
EXES := $(basename $(SRCS))
PYTEST_OPTIONS := --doctest-modules --quiet 


## Pattern rules
%: %.py
	chmod +x $<
	ln $< $@


## Default (first) target
all: test


## Generic rules
clean:
	rm $(EXES)
distclean:
	git clean -dfxq
test:
	PYTHONPATH=$$PWD pytest $(PYTEST_OPTIONS)


## Pragmata
.PHONY: all clean distclean test
.SILENT:
